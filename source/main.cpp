/*
 * fker:   Brainfuck Interpreter
 * Version: 1.5
 *
 * File: main.cpp
 * Author: Alister Sanders
 * Date Modified:   29/08/14
 *
 */

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <algorithm>
#include "include/Brain.h"
using namespace std;

void printHelpMessage();
void printVersionInfo();
void multipleUsageMessage(string arg);

int main(int argc, char* argv[])
{ 
    SessionDetails details;
    vector<string> fileNames;

    for (int i = 1; i < argc; i++)
    {
	string arg = argv[i];

	if (arg == "--version" | arg == "-v")
	{
	    printVersionInfo();
	    return 0;
	} else if (arg == "--help" || arg == "-h")
	{
	    printHelpMessage();
	    return 0;
	} else if (arg == "--trace" || arg == "-t")
	{
	    if (details.debug)
	    {
		multipleUsageMessage(arg);
	    } else {
		details.debug = true;
	    }

	} else if (arg == "--quiet" || arg == "-q")
	{
	    if (details.quiet)
	    {
		multipleUsageMessage(arg);
	    } else {
		details.quiet = true;
	    }

	} else if ((arg == "--delay" || arg == "-d") && i + 1 < argc)
	{
	    if (details.execDelay)
	    {
		multipleUsageMessage(arg);
	    } else {
		if ([](string s) -> bool { for (char c : s) { if (!(c > 47 && c < 58)) return false; } return true; }(string(argv[i + 1]))) // Make sure the next arg is numeric
		{
		    details.execDelay = atoi(argv[i + 1]);
		    
		    if (details.execDelay > MAX_EXECUTION_DELAY || details.execDelay < 0)
		    {
			cout << "Unable to set execution delay to " << argv[i + 1] 
			     << ": value too " << (details.execDelay < 0 ? "small" : "large") 
			     << ".\nDefaulting to " << (details.execDelay < 0 ? 0 : MAX_EXECUTION_DELAY) << " milliseconds\n" << endl;
			details.execDelay = details.execDelay < 0 ? 0 : MAX_EXECUTION_DELAY;
		    }

		    i++;
		} else
		{
		    cout << "Invalid timeframe '" << argv[i + 1] << "'" << endl;
		    return -1;
		}
	    }
	} else if (arg == "--delay" || arg == "-d")
	{
	    cout << "--delay <milliseconds>" << endl;
	    return -1;
	} else if (arg == "--ide-mode") details.ideMode = true;
	else {
	    if ([](char* file) -> bool { return ifstream(file); }(argv[i]))
	    {	
		fileNames.push_back(argv[i]);
	    }
	    else
	    {
		cout << "File does not exist: " << argv[i] << endl;
		return -1;
	    }
	}
    }

    if (fileNames.size() < 1)
    {
	printf("Error: No input file(s)\n");
	return -1;
    }

    Brain *mainBrain = new Brain(details); // Declare as pointer to reset with each file
    string bfStrings[fileNames.size()];
    string buffer;
    ifstream inputFiles[fileNames.size()];

    for (int i = 0; i < fileNames.size(); i++)
    {
	try {
	    inputFiles[i].open(fileNames[i]);
	} catch (string s) {
	    cout << "Error opening file " << fileNames[i] << "\n" << s << endl;
	    return -1;
	}

	if (!inputFiles[i].is_open())
	{
	    cout << "Unable to open file " << fileNames[i] << endl;
	    return -1;
	}
    }

    for (int i = 0; i < fileNames.size(); i++)
    {
	while (getline(inputFiles[i], buffer))
	{
	    bfStrings[i] += buffer;
	}
    }

    mainBrain->printsErrors(!details.quiet);

    for (int n = 0; n < fileNames.size(); n++)
    {
	if (fileNames.size() > 1)
	    printf("%sFile: %s\n", n == 0 ? "" : "\n\n", fileNames[n].c_str());

	for (int i = 0, j = 0; i < bfStrings[n].length(); i++, j++)
	{
	    bool skipFile = false;
	    mainBrain->processCode(bfStrings[n], &i, j);
	    
	    if (mainBrain->hasError() && !details.quiet && !details.ideMode)
	    {
		printf("Error detected. Continue? (Y/N/!) ");
		char choice = CP_getChar();

		while (choice != 'y' && choice != 'Y' && choice != 'n' && choice != 'N' && choice != '!')
		{
		    if (choice == 3) return 0;
		    printf("\rError detected. Continue? (Y/N/!)      \b\b\b\b\b");

		    choice = CP_getChar();
		}
	    
		putchar('\n');

		switch (choice)
		{
		    case 'y':
		    case 'Y': mainBrain->hasError(false); break;
		    case 'n':
		    case 'N': skipFile = true; break;
		    case '!': details.quiet = true; mainBrain->hasError(false); break;
		}

		if (skipFile)
		{
		    skipFile = false;
		    break;
		}
	    }
	}

	// Reset mainBrain for the next file
	delete mainBrain;
	mainBrain = new Brain(details);
    }
    
    return 0;
}

void printHelpMessage()
{
    cout << "\nUSAGE: \n fker [<filename(s)> [--trace] [--quiet] [--delay <milliseconds>] [--help] [--version]]\n\n";
    cout << "\t--trace\t\t Writes stack trace to fker_dbg.txt (-t)" << endl;
    cout << "\t--quiet\t\t Suppresses error messages (-q)" << endl;
    cout << "\t--delay\t\t Delays <milliseconds> after each instruction (-d)" << endl;
    cout << "\t--help\t\t Prints the help message (-h)" << endl;
    cout << "\t--version\t Displays version information about fker (-v)\n\n";
}

void printVersionInfo()
{
    cout << "fker Version: " << FKER_VERSION << endl;
    cout << "Author: Alister Sanders\n\n";
    cout << "See license.txt for license information" << endl;
}

void multipleUsageMessage(string arg)
{
    cout << "Multiple usage of '" << arg << "' flag." << endl;
}
