/*
 * fker:   Brainfuck Interpreter
 * Version: 1.5
 *
 * File: Brain.cpp
 * Author: Alister Sanders
 * Date Modified:   3/09/14
 *
 */

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include "../include/Brain.h"
using namespace std;

Brain::Brain(SessionDetails details)
{
    session = details;
    containsError = false;
    pointerLocation = 0;
    numberOfCells = 1;

    delete[] memoryCells;
    memoryCells = new uint8_t[numberOfCells];
}

Brain::~Brain()
{
    delete[] memoryCells;
    delete debugFile;
    
    memoryCells = NULL;
    debugFile = NULL;
}

bool Brain::hasError() { return containsError; }
void Brain::hasError(bool error) { containsError = error; }
void Brain::printsErrors(bool errors) { session.quiet = !errors; }

void Brain::processCode(string bfString, int *stringPos, int charNumber)
{
    int location = (*stringPos);
    switch ((int)bfString[location])
    {
        case 35: // #
	{
	    int numberOfDigits = 0;
	    string cellNumber;

	    if (location + 6 > bfString.length())
	    {
		printf("Unexpected EOF\n");
		this->printErrorLocation(&bfString, location);
		errorLocations.push_back(location);
		break;
	    }

	    for (int j = location + 1; j <= location + 5; j++)
	    {
		// Check to see if there are five digits from 0-9
		if ((int)bfString[j] > 47 && (int)bfString[j] < 58) numberOfDigits++;
		else break;
	    }

	    if (numberOfDigits < 5)
	    {
		printf("\nError dumping content: Expected 5 digits, Found %i.", numberOfDigits);
		this->printErrorLocation(&bfString, location, false);
		errorLocations.push_back(location);
		location += numberOfDigits;
		(*stringPos) = location;
		break;
	    }

	    // Get the cell number and put it into a string for later conversion
	    for (int j = location + 1; j < location + 6; j++)
	    {
		cellNumber += bfString[j];
	    }

	    // Check the cell number for errors
	    if (atoi(cellNumber.c_str()) > numberOfCells)
	    {
		printf("Index out of bounds");
		this->printErrorLocation(&bfString, location);
		errorLocations.push_back(location);
		location += 5;
		(*stringPos) = location;
		break;
	    }

	    DUMP_CELL(atoi(cellNumber.c_str()), memoryCells[atoi(cellNumber.c_str())]);
	    
	    // Add 5 to location to ignore the trailing characters
	    location += 5;

	    break;
	}

        case 36: // $
	    for (int i = 0; i <= numberOfCells; i++)
	    {
		if ((int)memoryCells[i] != 0)
		    DUMP_CELL(i, memoryCells[i]);
	    }
	    break;

        case 43: // +
	    ++memoryCells[pointerLocation];
	    break;

        case 44: // ,
	    memoryCells[pointerLocation] = CP_getChar();
	    putchar('\n');
	    break;

        case 45: // -
	    --memoryCells[pointerLocation];	    
	    break;

        case 46: // .
	    putchar(memoryCells[pointerLocation]);
	    break;

        case 60: // <
	    pointerLocation = pointerLocation - 1 < 0 ? numberOfCells : pointerLocation - 1;

	    // Remove cells if needed
	    // if (pointerLocation + 1 < numberOfCells && !memoryCells[numberOfCells])
	    // {
	    // 	numberOfCells--;
	    // 	uint8_t *temp = memoryCells;
		
	    // 	delete[] memoryCells;
	    // 	memoryCells = new uint8_t[numberOfCells];

	    // 	for (int i = 0; i < numberOfCells; i++)
	    // 	    memoryCells[i] = temp[i];
	    // 	}

	    break;

        case 62: // >
	    pointerLocation++;

	    // Manage dynamically add cells if needed
	    if (pointerLocation + 1 > numberOfCells)
	    {
		uint8_t *temp = memoryCells;
		numberOfCells++;
		
		memoryCells = new uint8_t[numberOfCells];

		for (int i = 0; i < numberOfCells; i++)
		    memoryCells[i] = temp[i];
		
		memoryCells[numberOfCells + 1] = 0;
	    }

	    break;

        case 91: // [
	{
	    if (location + 1 >= bfString.length())
	    {
		this->printErrorLocation(&bfString, location);
		errorLocations.push_back(location);
		break;
	    }

	    bool finish = false;
	    int noOfOpenBrackets = 0;
	    string subCode = "";

	    for (int j = location + 1; j < bfString.length(); j++)
	    {
		noOfOpenBrackets += (int)bfString[j] == 91 ? 1 : 0;

		if ((int)bfString[j] == 93 && noOfOpenBrackets > 0)
		{
		    noOfOpenBrackets--;
		} else if ((int)bfString[j] == 93 && noOfOpenBrackets == 0)
		{
		    finish = true;
		}

		if (finish) break;
		else subCode += bfString[j];
	    }

	    if (finish && memoryCells[pointerLocation])
	    {
		// Recursively process code within the loop
		int ptrLoc = pointerLocation;
		while (true)
		{
		    for (int j = 0; j < subCode.length(); j++)
		    {
			this->processCode(subCode, &j, charNumber);
		    }
		    
		    if (memoryCells[ptrLoc] == 1) break;
		}

		break;
	    } else if (!finish) {
		this->printErrorLocation(&bfString, location);
		errorLocations.push_back(location);
		break;
	    }
	}

        case 93: // ]
	{
	    int noOfOpenBrackets = 0;
	    bool finish = false;
	    
	    for (int j = location - 1; j > 0; j--)
	    {
		noOfOpenBrackets += (int)bfString[j] == 93 ? 1 : 0;
		
		if ((int)bfString[j] == 91 && noOfOpenBrackets > 0)
		{
		    noOfOpenBrackets--;
		} else if ((int)bfString[j] == 91 && noOfOpenBrackets == 0)
		{
		    finish = true;
		}

		if (finish) break;
	    }

	    if (finish) break;
	    else 
	    {
		this->printErrorLocation(&bfString, location);
		errorLocations.push_back(location);
		break;
	    }
	}
    }
    
    this->writeStacktrace((int)bfString[location], charNumber);
    (*stringPos) = location;

    CP_sleepMs(session.execDelay);
}

void Brain::printErrorLocation(string *bfString, int position, bool printMessage)
{
    if (session.quiet && !session.ideMode) return;

    if (printMessage) fprintf(stderr, "\nUnexpected token '%c'\n", (*bfString)[position]);
    else putchar('\n');

    for (int i = position - 5; i < position + 6; i++)
    {
	printf("%c", i < 0 || i > bfString->length() ? '`' : (char)(*bfString)[i]);
    }

    printf("\n     ^ Char %i\n", position + 1);

    containsError = true;
}

void Brain::writeStacktrace(int instruction, int charNumber)
{
    if (!session.debug || session.ideMode) return;

    if (!debugFile)
    {
	debugFile = fopen(DEBUG_FILENAME, "w");

	if (!debugFile)
	{
	    printf("Unable to open %s", DEBUG_FILENAME);
	    session.debug = false;
	    return;
	}
    }

    string debugMessage;

    char cellNumber[20];
    char memory[32];
    char character = (char)memoryCells[pointerLocation];
    sprintf(cellNumber, "%i", pointerLocation);
    sprintf(memory, "%i", (int)memoryCells[pointerLocation]);
    
    switch (instruction)
    {
	case 43:
	    debugMessage = "INC       @ CELL " + (string)cellNumber + " -> " + (string)memory;
	    break;
	case 44:
	    debugMessage = "PRMPT     @ CELL " + (string)cellNumber + " -> " + (string)memory;
	    break;
	case 45:
	    debugMessage = "DEC       @ CELL " + (string)cellNumber + " -> " + (string)memory;
	    break;
	case 46:
	    debugMessage = "OUT       @ CELL " + (string)cellNumber + " -> `" + character + "'";
	    break;
	case 60:
	    debugMessage = "DEC PTR ->  CELL " + (string)cellNumber;
	    break;
	case 62:
	    debugMessage = "INC PTR ->  CELL " + (string)cellNumber;
	    break;
	case 91:
	    debugMessage = "LOOP      @ CELL " + (string)cellNumber;
	    break;
	case 93:
	    debugMessage = "END LP    @ CELL " + (string)cellNumber;
	    break;

	default: return;
    }

    char chNum[32]; // detects a maximum program size of 9e+32 bytes
    sprintf(chNum, "%i", charNumber);

    for (int i = 0; i < errorLocations.size(); i++)
    {
	if (charNumber == errorLocations[i])
	{
	    debugMessage = "ERR       ";
	    break;
	}
    }

    debugMessage += ", CHR " + (string)chNum + "\n";

    fprintf(debugFile, debugMessage.c_str());

    if (ferror(debugFile))
    {
	session.debug = false;
	printf("Unable to write to %s: %s", DEBUG_FILENAME, strerror(errno));
    }
}
