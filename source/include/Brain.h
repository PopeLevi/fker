/*
 * fker:   Brainfuck Interpreter
 * Version: 1.5
 *
 * File: Brain.h
 * Author: Alister Sanders
 * Date Modified:   3/09/14
 *
 */

#ifndef BRAIN_H
#define BRAIN_H
#include <string>
#include <vector>
#include "defines.h"

class Brain
{
    public:
        Brain(SessionDetails details);
	~Brain();
	bool hasError(void);
	void hasError(bool error);
        void processCode(std::string bfString, int *stringPos, int charNumber);
	void printErrorLocation(std::string *bfString, int position, bool printMessage = true);
	void writeStacktrace(int instruction, int charNumber);
	void printsErrors(bool errors);

    private:
	FILE *debugFile = NULL;
        int pointerLocation;
	bool debug;
        uint8_t *memoryCells = NULL;
	int numberOfCells;
	std::vector<int> errorLocations;
	bool containsError;
	bool printErrors;
	SessionDetails session;
};

#endif
