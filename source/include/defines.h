/*
 * fker:   Brainfuck Interpreter
 * Version: 1.5
 *
 * File: defines.h
 * Author: Alister Sanders
 * Date:   26/08/14
 *
 */

#include "Methods.h"

#ifndef FKER_VERSION
#define FKER_VERSION "1.5"
#endif

#ifndef MAX_CELL_INDEX4
#define MAX_CELL_INDEX 30000
#endif

#ifndef DEBUG_FILENAME
#define DEBUG_FILENAME "fker_dbg.txt"
#endif

#ifndef MAX_EXECUTION_DELAY
#define MAX_EXECUTION_DELAY 8000
#endif

#define DUMP_CELL(cellNo, data) printf("\nDBG: #%05i:\t%05i\t%#04x\n", cellNo, data, data)
