/*
 * fker:   Brainfuck Interpreter
 * Version: 1.5
 *
 * File: Methods.h
 * Author: Alister Sanders
 * Date Modified:   26/08/14
 *
 */

#if defined _WIN32

#include <conio.h>
#include <windows.h>
#define CP_getChar() (uint8_t)_getche()
#define CP_sleepMs(ms) Sleep(ms)

#elif defined __linux || defined __unix || defined __APPLE__

#define CP_getChar() getChar_nix()
#define CP_sleepMs(ms) usleep(ms * 1000)

#include <cstdlib>
#include <unistd.h>
#include <termios.h>

inline char getChar_nix()
{
    struct termios t;
    tcgetattr(STDIN_FILENO, &t);
    t.c_lflag &= ~ICANON;
    tcsetattr(STDIN_FILENO, TCSANOW, &t);
    
    return std::cin.get();
}
#else

#include <cstdio>
#define CP_getChar() getchar()

#endif

#ifndef SESSION_DETAILS
#define SESSION_DETAILS

struct SessionDetails
{
    bool debug = false;
    bool quiet = false;
    bool ideMode = false;
    int execDelay = 0;
};

#endif
