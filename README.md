![fkerlogo_small.png](https://bitbucket.org/repo/GRkrj9/images/3891975153-fkerlogo_small.png)
# **fker** #

fker is a functional, cross-platform Brainfuck interpreter and debugger

## NOTE ##
fker is currently broken, and Brainfuck programs executed with the interpreter will not behave as expected.


## === Compilation === ##
To compile fker, simply download the repository, extract, cd to the /source directory and run g++ on main.cpp and Brain.cpp
      
```
#!bash

$ cd /location/of/extracted/files/source
$ g++ main.cpp src/Brain.cpp -ofker -std=gnu++11
```

Easy!
_____________________________________________________________________________________________________________________________________________________________

## === Pre-compiled binaries === ##
Pre-compiled binaries are available for download from the downloads page, oddly enough. MS Windows binaries are available pre-compiled, while Linux packages contain a Makefile (run `make install' to install)
_____________________________________________________________________________________________________________________________________________________________


## === Usage === ##
To run a Brainfuck program using fker use


```
#!bash

$ fker <filename>
```
_____________________________________________________________________________________________________________________________________________________________

## === Extra Instructions === ##

The fker implementation of Brainfuck implements two non-standard instructions:

### $ ###
Dumps a list of memory cells which do not contain 0 onto the screen

### #*xxxxx* ###
Dumps the decimal value of whatever the cell *xxxxx* contains

Example:

```
#!brainfuck

++++++++++>++++++--+>++++
#00000
#00001
#00002
```

Output

```
#!plaintext

 00000: 10
 00001: 5
 00002: 4

```
*Note: When using this instruction, be sure to have exactly 5 digits following the hash as the rest of the program ignores the 5 characters after the hash*
____________________________________________________________________________________________________________________________________________________________